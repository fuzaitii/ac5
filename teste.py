import unittest

from AC2 import app

class AC2Test(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()    
    
    def test_1(self):
        response = self.app.get('/')
        self.assertEqual("Se o senhor estiver vendo isso é porque funcionou, tenha um bom dia", response.get_data(as_text=True)
                         , "Deu erro !!!")
        
    def test_2(self):
        response = self.app.get('/teste1')
        self.assertEqual("teste 1", response.get_data(as_text=True)
                         , "Deu erro !!!")

    def test_3(self):
        response = self.app.get('/teste2')
        self.assertEqual("teste 2", response.get_data(as_text=True)
                         , "Deu erro !!!")

    def test_4(self):
        response = self.app.get('/teste3')
        self.assertEqual("teste 3", response.get_data(as_text=True)
                         , "Deu erro !!!")   

    def test_5(self):
        response = self.app.get('/teste4')
        self.assertEqual(404, response.status_code, "Deu erro !!!")

           
if __name__ == "__main__":
    unittest.main() 
         





