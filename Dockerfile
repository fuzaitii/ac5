FROM python:3-alpine3.15

WORKDIR /app

COPY . /app

CMD python ./AC2.py

RUN pip3 install -r requirements.txt

EXPOSE 3000


